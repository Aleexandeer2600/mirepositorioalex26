<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script src="jquery-3.4.1.min.js"></script>
    <script src="ajax.js"></script>
    <!-- <script src="ajax.js"></script>-->
    <title>UPTrans - LOGIN</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet"/>
    <link href="bootstrap/bootstrap-theme.css" rel="stylesheet"/>
    
</head>
<style>
    @font-face {
        font-family: Fuentechida;
        src: url(fonts/OleoScript-Regular.ttf);
    }

    body {
        background-image: url(imagenes/tela.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
    }

    .form {
        transition: 2s;
        margin-top: 100px;
        width: 30%;
        box-shadow: 0px 0px 40px rgba(213, 0, 0, 1), 0px 0px 80px rgba(256, 256, 256, 1);
    }

    .formulario:hover {
        transition: .8s;
        background-color: rgba(0, 0, 0, .5);
    }


    .logo {
        height: 75px;
        margin-top: 40px;
    }

    .espaciado {
        margin-top: 40px;
    }

    fieldset {
        transition: 2s;
        margin-bottom: 50px;
        border-color: rgb(64, 255, 140);
        border-style: groove;
        border-width: 5px;
        border-radius: 20px;
    }


    h3, h4 {

        color: white;
        text-align: center;
        font-family: fuentechida;
    }

    .Input {
        transition: .8s;
        background-color: rgba(0, 0, 0, .5);
        color: white;
        border-color: #006;
        border-bottom-color: white;
        border-bottom-style: groove;
        border-left: none;
        border-right: none;
        border-top: none;
        border-width: 4px;

    }

    .Input:hover {
        transition: .8s;
        background-color: rgba(55, 71, 79, .5);
        box-shadow: inset;
        border-bottom-color: #40ff8c;
    }

    .Input:focus {
        transition: .8s;
        border-bottom-color: lightslategray;
    }


    @media screen and (max-width: 750px) {

        .logo {
            height: 50px;

        }

        .formulario {
            transition: 2s;
            width: 95%;
            margin-top: 10px;
        }
    }

    button{
        font-family: Fuentechida;
    }

</style>

<body>
<div class="container formulario">
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4  ">
            <img src="imagenes/alumno.png" class="logo center-block">
        </div>
    </div>
    <div class=" espaciado">

    </div>
    <div class="row">
        <fieldset class="col-xs-10 col-xs-offset-1">
            <legend class="hidden-xs">
                <h3>inicio de sesi&oacute;n.</h3>
                <h4>ALUMNOS:</h4>
            </legend>
           <!-- Login alumno -->
            <form class="form-horizontal" method="post" action="/UPTrans/?controller=Usuario&action=loginAlum" id="login">

                <div class="form-group">
                    <label class="col-xs-12" for="nombre"><h4>Ingresa tu nombre</h4></label>
                    <div class="col-xs-10 col-xs-offset-1">
                        <input type="text" id="nombre" class="form-control Input"  placeholder="Nombre">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12" for="id_alumno"><h4>Ingresa tu matricula</h4></label>
                    <div class="col-xs-10 col-xs-offset-1">
                        <input type="password" id="id_alumno" class="form-control col-xs-12 Input" placeholder="Matriculaaa">
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Enviar" onclick="loginAlum()" class="btn btn-success center-block">
                </div>
                <p>¿Eres nuevo? <a href="RegistroAlumno.html"> Registrate aquí! </a> E inicia sesión después..! </p>
            </form>
        </fieldset>

    </div>
</div>

</section>

<footer id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <p>Si deseas ver el mapa del sitio <a href="mapa.html"> Da click aquí! </a> </p>
                </div>
            </div>
        </div>
    </div>
</footer>



<script src="js/vendor/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js" integrity="sha384-XTs3FgkjiBgo8qjEjBk0tGmf3wPrWtA6coPfQDfFEY8AnYJwjalXCiosYRBIBZX8" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js" integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK" crossorigin="anonymous"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>

</body>
<!-- Funcion ajax -->
<script>
    $(document).ready(function loginAlum() {
        $("#login [type='submit']").on("click", function (e) {
            e.preventDefault();
            var nombre = $("#login input[name='nombre']");
            var id_alumno = $("#login input[name='id_alumno']");
            if (nombre.val() == "" || id_alumno.val() == "" || nombre.val.status() == "error" || id_alumno.status() == "error") {
                alert("Error, favor de verificar datos");
                return;
            }else{
                alert("Bienvenido alumno "+ nombre);
                window.location.replace("Alumno.html");
            }
            $("#login").submit();
        });
    });
</script>
</html>
