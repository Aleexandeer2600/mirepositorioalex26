$(obtener_registros());
function obtener_registros(alumnos)
{
    $.ajax({
        url : "http://localhost/UPTrans/index.php?controller=Usuario&action=busqueda_tabla",
        type : 'POST',
        dataType : 'html',
        data : { alumnos: alumnos },
    })
        .done(function(resultado){
            console.log(resultado);
            $("#tabla_resultado").html(resultado);
        })
}
$(document).on('keyup', '#busqueda', function()
{
    var valorBusqueda=$(this).val();
    if (valorBusqueda!="")
    {
        obtener_registros(valorBusqueda);
    }
    else
    {
        obtener_registros();
    }
});