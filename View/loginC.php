<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <script src="jquery-3.4.1.min.js"></script>
    <script src="ajax.js"></script>
    <!-- <script src="ajax.js"></script>-->
    <title>UPTrans - LOGIN</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/bootstrap.min.css" rel="stylesheet"/>
    <link href="bootstrap/bootstrap-theme.css" rel="stylesheet"/>


</head>
<style>
    @font-face {
        font-family: Fuentechida;
        src: url(fonts/OleoScript-Regular.ttf);
    }

    button{
        font-family: Fuentechida;
    }


    body {
        background-image: url(imagenes/chofer.jpg);
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center;
    }

    .form {
        transition: 2s;
        margin-top: 100px;
        width: 30%;
        box-shadow: 0px 0px 40px rgb(102, 21, 24), 0px 0px 80px rgba(256, 256, 256, 1);
    }

    .formulario:hover {
        transition: .8s;
        background-color: rgba(0, 0, 0, .5);
    }


    .logo {
        height: 75px;
        margin-top: 40px;
    }

    .espaciado {
        margin-top: 40px;
    }

    fieldset {
        transition: 2s;
        margin-bottom: 50px;
        border-color: rgb(102, 21, 24);
        border-style: groove;
        border-width: 5px;
        border-radius: 20px;
    }


    h3, h4 {

        color: white;
        text-align: center;
        font-family: fuentechida;
    }

    .Input {
        transition: .8s;
        background-color: rgba(0, 0, 0, .5);
        color: white;
        border-color: #661518;
        border-bottom-color: white;
        border-bottom-style: groove;
        border-left: none;
        border-right: none;
        border-top: none;
        border-width: 4px;

    }

    .Input:hover {
        transition: .8s;
        background-color: rgba(55, 71, 79, .5);
        box-shadow: inset;
        border-bottom-color: #661518;
    }

    .Input:focus {
        transition: .8s;
        border-bottom-color: #661518;
    }


    @media screen and (max-width: 750px) {

        .logo {
            height: 50px;

        }

        .formulario {
            transition: 2s;
            width: 95%;
            margin-top: 10px;
        }
    }
</style>


<body>
<div class="container formulario">
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4  ">
            <img src="imagenes/chofer.png" class="logo center-block">
        </div>

    </div>
    <div class=" espaciado">

    </div>
    <div class="row">
        <fieldset class="col-xs-10 col-xs-offset-1">


            <legend class="hidden-xs">
                <h3>inicio de sesi&oacute;n.</h3>
                <h4>Choferes:</h4>
            </legend>

            <form role="form" class="form-horizontal" method="post" action="/UPTrans/?controller=Usuario&action=loginChof" id="login">

                <div class="form-group">
                    <label class="col-xs-12" for="correo"><h4>Ingresa tu correo</h4></label>
                    <div class="col-xs-10 col-xs-offset-1">
                        <input type="text" id="correo" class="form-control Input"  placeholder="Correo">
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-xs-12" for="contrasena"><h4>Ingresa tu contraseña</h4></label>
                    <div class="col-xs-10 col-xs-offset-1">
                        <input type="password" id="contrasena" class="form-control col-xs-12 Input" placeholder="Contraseña">
                    </div>
                </div>

                <div class="form-group">

                    <button type="button" value="Enviar" onclick="loginChof()" class="btn btn-danger center-block">Aceptar</button>

                </div>
                <p>¿Eres nuevo? <a href="RegistroChofer.html"> Registrate aquí! </a> E inicia sesión después..! </p>
            </form>
        </fieldset>

    </div>
</div>
</body>
<!-- Funcion ajax -->
<script>
    $(document).ready(function loginChof() {
        $("#login [type='submit']").on("click", function (e) {
            e.preventDefault();
            var correo = $("#login input[name='correo']");
            var contrasena = $("#login input[name='contrasena']");

            if (correo.val() == "" || contrasena.val() == "" correo.val.status() == "error" || contrasena.status() == "error") {
                alert("Error, favor de verificar datos");
                return;
            }else{
                alert("Bienvenido chofer ");
                window.location.replace("choferes.html");
            }
            $("#login").submit();
        });
    });
</script>
</html>
