function loginAlum() {
    //----Mandas el valor de la caja de texto a una variable
    var i = document.getElementById("id_alumno").value
    var n = document.getElementById("nombre").value


    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=loginAlum";
    $.ajax({
        type: "post",
        url: url,
        data: {
            id_alumno: i,
            nombre: n
        },
        dataType: "json",
        success: function (data) {
            console.log(data);
            //Validacion para datos de usuarios
            if (data.status === "error") {           //Si el "status" me devuelve un error entrara a esta condicion
                alert("Error, favor de verificar datos");
            } else {                                  //Si la condicion anterior no se cumple entrara los datos son correctos
                alert("Bienvenido alumno "+n);
               window.location.replace("Alumno.html");
            }
        }
    })

}

function vuelveAlum() {
    window.location.replace("loginA.php");
}

function alumno() {
    //----Mandas el valor de la caja de texto a una variable
    // var i = document.getElementById("id_alumno").value
    var m = document.getElementById("matricula").value
    var n = document.getElementById("nombre").value
    var c = document.getElementById("carrera_id").value
    var g = document.getElementById("grupo").value
    var s = document.getElementById("sexo").value
    var cua = document.getElementById("cuatrimestre").value
    var r = document.getElementById("ruta_id").value
    var u = document.getElementById("unidad_id").value

    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=createAlum";
    $.ajax({
        type: "post",
        url: url,
        data: {
            // id_alumno: i,
            matricula: m,
            nombre: n,
            carrera_id: c,
            grupo: g,
            sexo: s,
            cuatrimestre: cua,
            ruta_id: r,
            unidad_id: u
        },
        success: function (datos) {
            console.log(datos);
            $("#RegistroAlumno").html(datos);
            if (datos.status === "true") {
                alert("Usuario no registrado");
            }else{
                alert("Usuario registrado");
                window.location.replace("loginA.php");

            }
        }
    })


}

function eliminarAlum() {
    var mat = document.getElementById("matricula").value

    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=eliminarAlum";

    $.ajax({
        type: "post",
        url: url,
        data: {matricula: mat},
        success: function (data) {
            $("#datosAlumno").html(data);
            console.log(data);
        }
    })
}

/////////////////////////////////////////////////////CHOFER////////////////////////////////////////////////////////
function vuelveChof() {
    window.location.replace("loginC.php");
}
function loginChof() {

    //----Mandas el valor de la caja de texto a una variable
    var con = document.getElementById("contrasena").value
    var co = document.getElementById("correo").value

    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=loginChof";
    $.ajax({
        type: "post",
        url: url,
        data: {
            contrasena: con,
            correo: co
        },
        dataType: "json",

        success: function (data) {
            console.log(data);
            //Validacion para datos de usuarios
            if (data.status === "error") {           //Si el "status" me devuelve un error entrara a esta condicion
                alert("Error, favor de verificar datos");
            } else {                                  //Si la condicion anterior no se cumple entrara los datos son correctos
                alert("Bienvenido chofer");
               window.location.replace("Choferes.html");
            }


        }
    })

}

function chofer() {
    //----Mandas el valor de la caja de texto a una variable
    // var i = document.getElementById("id_chofer").value
    var n = document.getElementById("nombre").value
    var d = document.getElementById("direccion").value
    var t = document.getElementById("telefono").value
    var u = document.getElementById("unidad_id").value
    var c = document.getElementById("correo").value
    var pass = document.getElementById("contrasena").value

    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=createChof";
    $.ajax({
        type: "post",
        url: url,
        data: {
            nombre: n,
            direccion: d,
            telefono: t,
            unidad_id: u,
            correo: c,
            contrasena: pass
        },
        dataType: "json",


        success: function (data) {
            console.log(data);
           // $("#RegistroChofer").html(datos);
            if (data.status === "error") {           //Si el "status" me devuelve un error entrara a esta condicion
                alert("Error, favor de verificar datos");
            } else {                                  //Si la condicion anterior no se cumple entrara los datos son correctos
                alert("Chofer creado, ya esta dado de alta");
               window.location.replace("LoginC.php");
            }
        }
    })
}

function eliminarChof() {

    var id = document.getElementById("id_chofer").value
    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=eliminarChof";

    $.ajax({
        type: "post",
        url: url,
        data: {id_chofer: id},
        success: function (data) {
            $("#datosChofer").html(data);
            console.log(data);
        }
    })

}

function loginAdm(){

    var nick = document.getElementById("nick").value
    var pass = document.getElementById("pass").value

    var url = "http://localhost/UPTrans/index.php?controller=Usuario&action=loginAdm";
    $.ajax({
        type: "post",
        url: url,
        data: {
            nick: nick,
            pass: pass
        },
        dataType: "json",

        success: function (data) {
            console.log(data);
            //Validacion para datos de usuarios
            if (data.status === "error") {           //Si el "status" me devuelve un error entrara a esta condicion
                alert("Error, favor de verificar datos");
            } else {                                  //Si la condicion anterior no se cumple entrara los datos son correctos
                alert("Bienvenido administrador "+ nick);
                window.location.replace("administrador.html");
            }


        }
    })

}


