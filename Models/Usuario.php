<?php

namespace Models;

class Usuario extends conexion
{
    // -- Atributos del alumno
    public $id_alumno;
    public $matricula;
    public $nombre;
    public $carrera_id;
    public $grupo;
    public $sexo;
    public $cuatrimestre;
    public $ruta_id;
    public $unidad_id;

    // -- atributos chofer
    public $id_chofer;
    public $nombrechof;
    public $direccion;
    public $telefono;
    public $unidad_id3;
    public $correo;
    public $contrasena;

    // -- funcioón insertar Alumno
    function insertA()
    {
        /*
         * obtiene la fecha
         $this->fecha_registro = date("Y-m-d");
         */
        //-- Sentencia SQL para agregar
        $pre = mysqli_prepare($this->con, "INSERT INTO alumno(matricula, nombre, carrera_id, grupo, sexo, cuatrimestre, ruta_id, unidad_id) VALUES (?,?,?,?,?,?,?,?)");
        /*
         * Preparamos los datos con Bind_param
         *  la s es de tipo string, i para int, b es blob (imagenes etc), d números flotantes, el número de cáracteres dependede los datos ingresados en el query.
         */
        $pre->bind_param("isissiii", $this->matricula, $this->nombre, $this->carrera_id, $this->grupo, $this->sexo, $this->cuatrimestre, $this->ruta_id, $this->unidad_id);
        // -- ejecutamos el query
        $pre->execute();
        /*
        $pre_ =mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id_Usuario");
        $pre_->execute();
        $r= $pre_->get_result();
        $this->id_Usuario= $r->fetch_assoc()["id_Usuario"];
        */

        return true;
    }

    // -- función insertar Chofer
    function insertC()
    {

        //-- Sentencia SQL para agregar
        $pre = mysqli_prepare($this->con, "INSERT INTO chofer(nombre, direccion, telefono, unidad_id3, correo, contrasena) VALUES (?,?,?,?,?,?)");
        /*
         * Preparamos los datos con Bind_param
         *  la s es de tipo string, i para int, b es blob (imagenes etc), d números flotantes, el número de cáracteres dependede los datos ingresados en el query.
         */
        $pre->bind_param("ssiiss", $this->nombrechof, $this->direccion, $this->telefono, $this->unidad_id3, $this->correo, $this->contrasena);
        // -- ejecutamos el query
        $pre->execute();



        return true;
    }

    // -- Buscar alumno por matricula
    /**
     * @param $matri
     * @return object|\stdClass
     */
    static function findA($id, $nom)
    {
        $me = new \Models\conexion();

        $pre = mysqli_prepare($me->con, "SELECT * FROM alumno WHERE matricula = ? and nombre = ?" );
        $pre->bind_param("is", $id,$nom);
        $pre->execute();
        $res = $pre->get_result();
       return $res->fetch_object();
    }
    static function findByEmail($email){
        $me = new conexion();
        $pre =  mysqli_prepare($me->con,"SELECT * FROM alumno WHERE matricula=?");
        $pre->bind_param("s",$email);
        $pre->execute();
        $re = $pre->get_result();
        echo json_encode($re);
        return $re->fetch_object(Usuario::class);
    }

    // -- Bucar chofer por id
    static function findChofi($co, $pass)
    {
        $me = new \Models\conexion();

        $pre = mysqli_prepare($me->con, "SELECT * FROM chofer WHERE correo = ? and contrasena = ?");
        $pre->bind_param("ss", $co, $pass);
        $pre->execute();
        $res = $pre->get_result();
       return $res->fetch_object();
    }

    static function findAdm($nick, $pass)
    {
        $me = new \Models\conexion();

        $pre = mysqli_prepare($me->con, "SELECT * FROM admin WHERE nick = ? and pass = ?" );
        $pre->bind_param("is", $nick,$pass);
        $pre->execute();
        $res = $pre->get_result();
        return $res->fetch_object();
    }

    // -- Muestra todos los alumnos
    static function All()
    {
        $me = new conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM alumno");
        $pre->execute();
        $res = $pre->get_result();
        $alum = [];
      /*  while ($Alumnos = $res->fetch_object(Usuario::class)) {
            array_push($alum, $Alumnos);
        }
        echo json_encode($alum);*/
    }

    // -- actualiza alumno
    function updateA($id, $nom)
    {
        //-- Sentencia SQL para agregar
        $pre = mysqli_prepare($this->con, "UPDATE alumno SET nombre = ? WHERE id_alumno = ?");

        $pre->bind_param("si", $nom, $id);
        // -- ejecutamos el query
        $pre->execute();

        return true;
    }

    // -- actualiza chofer
    function updateC($id, $nom)
    {
        //-- Sentencia SQL para agregar
        $pre = mysqli_prepare($this->con, "UPDATE chofer SET  nombre = ? WHERE id_chofer = ?");

        $pre->bind_param("si", $nom, $id);
        // -- ejecutamos el query
        $pre->execute();

        return true;
    }

    // -- borra alumno
    function deleteA($matri)
    {
        //-- Sentencia SQL para agregar
        $pre = mysqli_prepare($this->con, "DELETE FROM alumno WHERE matricula = ?");

        $pre->bind_param("i", $matri);
        // -- ejecutamos el query
        $pre->execute();

        return true;
    }

    // -- borra chofer
    function deleteC($id)
    {
        //-- Sentencia SQL para agregar
        $pre = mysqli_prepare($this->con, "DELETE FROM chofer WHERE id_chofer = ?");

        $pre->bind_param("i", $id);
        // -- ejecutamos el query
        $pre->execute();

        return true;
    }




}

